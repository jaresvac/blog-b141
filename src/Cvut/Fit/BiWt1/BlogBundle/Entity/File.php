<?php
/**
 * Created by PhpStorm.
 * User: jares
 * Date: 13/10/14
 * Time: 11:01
 */

namespace Cvut\Fit\BiWt1\BlogBundle\Entity;


use Cvut\Fit\BiWt1\BlogBundle\Entity\Attribute\CreatedAt;
use Cvut\Fit\BiWt1\BlogBundle\Entity\Attribute\Identifier;

class File implements FileInterface
{

	use Identifier;
	use CreatedAt;

	protected $name;

	protected $post;

	protected $internetMediaType;

	protected $data;

	protected $comment;

	/**
	 * Vrati jmeno souboru
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Nastavi jmeno souboru
	 *
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * Vrati zapisek, k nemuz soubor prislusi
	 *
	 * @return \Cvut\Fit\BiWt1\BlogBundle\Entity\PostInterface
	 */
	public function getPost()
	{
		return $this->post;
	}

	/**
	 * Nastavi zapisek, k nemuz soubor prislusi
	 *
	 * @param \Cvut\Fit\BiWt1\BlogBundle\Entity\PostInterface $post
	 */
	public function setPost($post)
	{
		$this->post = $post;
	}

	/**
	 * Vrati Internet Media Type souboru (drive MIME type)
	 *
	 * @return string
	 */
	public function getInternetMediaType()
	{
		return $this->internetMediaType;
	}

	/**
	 * Nastavi Internet Media Type souboru (drive MIME type)
	 *
	 * @param string $internetMediaType
	 */
	public function setInternetMediaType($internetMediaType)
	{
		$this->internetMediaType = $internetMediaType;
	}

	/**
	 * Vrati obsah souboru
	 *
	 * @return mixed
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * Nastavi obsah souboru
	 *
	 * @param mixed $data
	 */
	public function setData($data)
	{
		$this->data = $data;
	}

	/**
	 * Vrati, k jakemu komentari soubor prislusi
	 *
	 * @return CommentInterface
	 */
	public function getComment()
	{
		return $this->comment;
	}

	/**
	 * Nastavi, k jakemu komentari soubor prislusi
	 *
	 * @param CommentInterface $comment
	 */
	public function setComment($comment)
	{
		$this->comment = $comment;
	}
}
