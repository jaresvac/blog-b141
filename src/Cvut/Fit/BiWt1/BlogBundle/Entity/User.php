<?php
/**
 * Created by PhpStorm.
 * User: jares
 * Date: 09/10/14
 * Time: 13:20
 */

namespace Cvut\Fit\BiWt1\BlogBundle\Entity;


use Cvut\Fit\BiWt1\BlogBundle\Entity\Attribute\Identifier;
use Doctrine\Common\Collections\ArrayCollection;

class User implements UserInterface
{

	use Identifier;

	protected $name;

	protected $posts;

	public function __construct()
	{
		$this->posts = new ArrayCollection();
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return mixed
	 */
	public function getPosts()
	{
		return $this->posts;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @param mixed $posts
	 */
	public function setPosts($posts)
	{
		$this->posts = $posts;
	}

	/**
	 * Prida zapisek uzivatele
	 * @param PostInterface $post
	 */
	public function addPost(PostInterface $post)
	{
		$this->posts->add($post);
	}

	/**
	 * Odstrani zapisek uzivatele
	 * @param PostInterface $post
	 */
	public function removePost(PostInterface $post)
	{
		$this->posts->removeElement($post);
	}
}
