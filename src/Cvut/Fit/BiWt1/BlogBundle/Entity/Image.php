<?php
/**
 * Created by PhpStorm.
 * User: jares
 * Date: 13/10/14
 * Time: 11:05
 */

namespace Cvut\Fit\BiWt1\BlogBundle\Entity;


class Image extends File implements ImageInterface
{
	protected $dimensionX;

	protected $dimensionY;

	protected $preview;

	/**
	 * Vrati rozliseni v ose X
	 *
	 * @return number
	 */
	public function getDimensionX()
	{
		return $this->dimensionX;
	}

	/**
	 * Nastavi rozliseni v ose X
	 * @param number $dimensionX
	 */
	public function setDimensionX($dimensionX)
	{
		$this->dimensionX = $dimensionX;
	}

	/**
	 * Vrati rozliseni v ose Y
	 *
	 * @return number
	 */
	public function getDimensionY()
	{
		return $this->dimensionY;
	}

	/**
	 * Nastavi rozliseni v ose Y
	 * @param number $dimensionY
	 */
	public function setDimensionY($dimensionY)
	{
		$this->dimensionY = $dimensionY;
	}

	/**
	 * Vrati data pro preview
	 *
	 * @return blob
	 */
	public function getPreview()
	{
		return $this->preview;
	}

	/**
	 * Nastavi data pro preview
	 *
	 * @param blob
	 */
	public function setPreview($preview)
	{
		$this->preview = $preview;
	}
}
