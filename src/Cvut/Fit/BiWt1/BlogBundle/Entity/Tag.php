<?php
/**
 * Created by PhpStorm.
 * User: jares
 * Date: 13/10/14
 * Time: 10:40
 */

namespace Cvut\Fit\BiWt1\BlogBundle\Entity;


use Cvut\Fit\BiWt1\BlogBundle\Entity\Attribute\Identifier;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Tag implements TagInterface{

	use Identifier;

	protected $title;

	/** @var  Collection */
	protected $posts;

	public function __construct()
	{
		$this->posts = new ArrayCollection();
	}

	/**
	 * Vrati nazev tagu
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Nastavi nazev tagu
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * Prida zapisek ke znacce
	 *
	 * @param PostInterface $post
	 */
	public function addPost(PostInterface $post)
	{
		$this->posts->add($post);
	}

	/**
	 * Vrati zapisky prirazene ke znacce
	 *
	 * @return Collection<PostInterface>
	 */
	public function getPosts()
	{
		return $this->posts;
	}

	/**
	 *
	 * Odstrani zapisek od znacky
	 *
	 * @param PostInterface $post
	 */
	public function removePost(PostInterface $post)
	{
		if($this->posts->contains($post)){
			$this->posts->removeElement($post);
		}
	}
}
