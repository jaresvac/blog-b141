<?php
/**
 * Created by PhpStorm.
 * User: jares
 * Date: 13/10/14
 * Time: 10:45
 */

namespace Cvut\Fit\BiWt1\BlogBundle\Entity;


use Cvut\Fit\BiWt1\BlogBundle\Entity\Attribute\Identifier;
use Cvut\Fit\BiWt1\BlogBundle\Entity\Attribute\Timestamps;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Comment implements CommentInterface
{

	use Identifier;
	use Timestamps;


	protected $author;

	protected $post;

	protected $parent;

	protected $text;

	/** @var  Collection */
	protected $files;

	protected $spam = FALSE;


	public function __construct()
	{
		$this->files = new ArrayCollection();
	}

	/**
	 * Vratui autora
	 *
	 * @return UserInterface
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * Nastavi autora
	 *
	 * @param UserInterface $author
	 */
	public function setAuthor(UserInterface $author)
	{
		$this->author = $author;
	}

	/**
	 * Vrati prispevek, ke kteremu komentar prislusi
	 *
	 * @return PostInterface
	 */
	public function getPost()
	{
		return $this->post;
	}

	/**
	 * Nastavi prispevek, ke kteremu komentar prislusi
	 *
	 * @param PostInterface $post
	 */
	public function setPost(PostInterface $post)
	{
		$this->post = $post;
	}

	/**
	 * Vrati text komentare
	 *
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}

	/**
	 * Nastavi text komentare
	 *
	 * @param string $text
	 */
	public function setText($text)
	{
		$this->text = $text;
	}

	/**
	 * Vrati rodicovsky komentar, pokud existuje
	 *
	 * @return CommentInterface|null
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * Nastavi rodicovsky komentar. Museji byt ke stejnemu zapisku.
	 *
	 * @param CommentInterface $parent
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
	}

	/**
	 * Prida soubor k tomuto komentari
	 *
	 * @param FileInterface $file
	 */
	public function addFile(FileInterface $file)
	{
		$this->files->add($file);
	}

	/**
	 * Vrati kolekci souboru prislusejicich tomuto komentari
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * Odebere soubor od tohoto komentare
	 *
	 * @param FileInterface $file
	 */
	public function removeFile(FileInterface $file)
	{
		if ($this->files->contains($file)) {
			$this->files->removeElement($file);
		}
	}

	/**
	 * Vrati priznak, zda se jedna o spam
	 *
	 * @return true|false
	 */
	public function getSpam()
	{
		return $this->spam;
	}

	/**
	 * Nastavi priznak, zda se jedna o spam
	 *
	 * @param boolean $spam
	 */
	public function setSpam($spam)
	{
		$this->spam = $spam;
	}
}
