<?php
/**
 * Created by PhpStorm.
 * User: jares
 * Date: 09/10/14
 * Time: 13:14
 */

namespace Cvut\Fit\BiWt1\BlogBundle\Entity;


use Cvut\Fit\BiWt1\BlogBundle\Entity\Attribute\Identifier;
use Cvut\Fit\BiWt1\BlogBundle\Entity\Attribute\Timestamps;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Post implements PostInterface
{

	use Identifier;
	use Timestamps;

	protected $title;

	protected $text;

	protected $author;

	protected $publishFrom;

	protected $publishTo;

	protected $private;

	/** @var  ArrayCollection */
	protected $files;

	/** @var ArrayCollection */
	protected $comments;

	/** @var ArrayCollection */
	protected $tags;

	public function __construct()
	{
		$this->comments = new ArrayCollection();
		$this->tags = new ArrayCollection();
		$this->files = new ArrayCollection();
	}

	/**
	 * @return mixed
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @return mixed
	 */
	public function getComments()
	{
		return $this->comments;
	}

	/**
	 * @return mixed
	 */
	public function getPrivate()
	{
		return $this->private;
	}

	/**
	 * @return mixed
	 */
	public function getPublishFrom()
	{
		return $this->publishFrom;
	}

	/**
	 * @return mixed
	 */
	public function getPublishTo()
	{
		return $this->publishTo;
	}

	/**
	 * @return mixed
	 */
	public function getTags()
	{
		return $this->tags;
	}

	/**
	 * @return mixed
	 */
	public function getText()
	{
		return $this->text;
	}

	/**
	 * @return mixed
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param mixed $author
	 */
	public function setAuthor(UserInterface $author)
	{
		$this->author = $author;
	}

	/**
	 * @param mixed $comments
	 */
	public function setComments($comments)
	{
		$this->comments = $comments;
	}

	/**
	 * @param mixed $private
	 */
	public function setPrivate($private)
	{
		$this->private = $private;
	}

	/**
	 * @param mixed $publishFrom
	 */
	public function setPublishFrom(\DateTime $publishFrom)
	{
		$this->publishFrom = $publishFrom;
	}

	/**
	 * @param mixed $publishTo
	 */
	public function setPublishTo(\DateTime $publishTo)
	{
		$this->publishTo = $publishTo;
	}

	/**
	 * @param mixed $text
	 */
	public function setText($text)
	{
		$this->text = $text;
	}

	/**
	 * @param mixed $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}


	/**
	 * Prida soubor k zapisku
	 *
	 * @param FileInterface $file
	 * @return PostInterface
	 */
	public function addFile(FileInterface $file)
	{
		$file->setPost($this);
		$this->files->add($file);
	}

	/**
	 * Odebere soubor od zapisku
	 *
	 * @param FileInterface $file
	 * @return PostInterface
	 */
	public function removeFile(FileInterface $file)
	{
		if ($this->files->contains($file)) {
			$this->files->removeElement($file);
		}
	}

	/**
	 * Vrati kolekci souboru prislusejicich k zapisku
	 *
	 * @return Collection<File>
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * Prida komentar k zapisku
	 *
	 * @param CommentInterface $comment
	 * @return PostInterface
	 */
	public function addComment(CommentInterface $comment)
	{
		$comment->setPost($this);
		$this->comments->add($comment);
	}

	/**
	 * Odebere komentar od zapisku
	 *
	 * @param CommentInterface $comment
	 * @return PostInterface
	 */
	public function removeComment(CommentInterface $comment)
	{
		if ($this->comments->contains($comment)) {
			$this->comments->removeElement($comment);
		}
	}

	/**
	 * Prida znacku k zapisku
	 *
	 * @param TagInterface $tag
	 * @return PostInterface
	 */
	public function addTag(TagInterface $tag)
	{
		$this->tags->add($tag);
	}

	/**
	 * Odebere znacku od zapisku
	 *
	 * @param TagInterface $tag
	 * @return PostInterface
	 */
	public function removeTag(TagInterface $tag)
	{
		if ($this->tags->contains($tag)) {
			$this->tags->removeElement($tag);
		}
	}
}
