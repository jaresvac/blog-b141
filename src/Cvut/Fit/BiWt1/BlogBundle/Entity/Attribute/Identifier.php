<?php
/**
 * Created by PhpStorm.
 * User: jares
 * Date: 09/10/14
 * Time: 13:15
 */

namespace Cvut\Fit\BiWt1\BlogBundle\Entity\Attribute;


Trait Identifier
{

	protected $id;

	/**
	 * Vrati id zapisku
	 *
	 * @return number
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Nastavi unikatni ID
	 *
	 * @param number $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}
} 
