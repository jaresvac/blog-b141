<?php
/**
 * Created by PhpStorm.
 * User: jares
 * Date: 13/10/14
 * Time: 10:47
 */

namespace Cvut\Fit\BiWt1\BlogBundle\Entity\Attribute;


Trait Timestamps
{
	use CreatedAt;

	protected $modified;


	/**
	 * @param \DateTime $modified
	 */
	public function setModified(\DateTime $modified)
	{
		$this->modified = $modified;
	}

	/**
	 * @return \DateTime
	 */
	public function getModified()
	{
		return $this->modified;
	}
} 
