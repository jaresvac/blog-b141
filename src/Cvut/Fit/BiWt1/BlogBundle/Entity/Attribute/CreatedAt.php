<?php
/**
 * Created by PhpStorm.
 * User: jares
 * Date: 13/10/14
 * Time: 10:55
 */

namespace Cvut\Fit\BiWt1\BlogBundle\Entity\Attribute;


Trait CreatedAt
{
	protected $created;

	/**
	 * @param \DateTime $created
	 */
	public function setCreated(\DateTime $created)
	{
		$this->created = $created;
	}


	/**
	 * @return \DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}
} 
