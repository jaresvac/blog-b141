<?php
/**
 * Created by PhpStorm.
 * User: kadleto2
 * Date: 1.10.14
 * Time: 16:55
 */

namespace Cvut\Fit\BiWt1\BlogBundle\DependencyInjection;


use Symfony\Component\Yaml\Parser;

/**
 * Trida zajistuje mapovani mezi rozhranim a tridou, ktera ho implementuje.
 *
 * Class InterfaceToClassMap
 * @package Cvut\Fit\BiWt1\BlogBundle\DependencyInjection
 */
class InterfaceToClassMap {

    protected $classes = [];

    /**
     * Inicializace mapovani mezi rozhrani a implementujici tridou
     *
     * @param $file cesta k souboru s mapovanim
     */
    public function __construct($file)
    {
        $parser = new Parser();
        $this->classes = $parser->parse(file_get_contents($file));
    }

    /**
     * Provede mapovani z FQN rozhrani na FQN tridy, ktera jej implementuje
     *
     * @param $interface FQN rozhrani
     * @return string FQN odpovidajici tridy
     * @throws \RuntimeException pokud rozhrani neni podporovano nebo pro nej neni nastaveno mapovani
     */
    public function getClass($interface)
    {

        
        if (interface_exists($interface)) {
            if (!(isset($this->classes['map'][$interface]) && !empty($this->classes['map'][$interface])))
                throw new \RuntimeException("Interface-to-class mapping is not configured for $interface.");
            if (!class_exists($this->classes['map'][$interface]))
                throw new \RuntimeException("Interface-to-class mapping is invalid for $interface. Class does not exist.");

            return $this->classes['map'][$interface];
        }
        throw new \RuntimeException("Interface $interface does not exist.");
    }

} 