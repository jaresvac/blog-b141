<?php
/**
 * Created by PhpStorm.
 * User: jares
 * Date: 09/10/14
 * Time: 13:30
 */

namespace Cvut\Fit\BiWt1\BlogBundle\Service;


use Cvut\Fit\BiWt1\BlogBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

class UserService implements UserInterface
{

	/**
	 * @var Collection
	 */
	protected $usersDao;

	public function __construct()
	{
		$this->usersDao = new ArrayCollection();
	}


	/**
	 * Vytvori a vrati uzivatele
	 *
	 * @param $id
	 * @param $name
	 * @return \Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface
	 */
	public function create($id, $name)
	{
		$user = new User();
		$user->setId($id);
		$user->setName($name);
		$this->usersDao->set($user->getId(), $user);

		return $user;
	}

	/**
	 * Aktualizuje uzivatele a vrati
	 *
	 * @param \Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface $user
	 * @return \Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface
	 */
	public function update(\Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface $user)
	{
		return $user;
	}

	/**
	 * Odstrani uzivatele a vrati jej
	 *
	 * @param \Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface $user
	 * @return \Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface
	 */
	public function delete(\Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface $user)
	{
		if ($this->usersDao->contains($user)) {
			$this->usersDao->removeElement($user);
		}
		return $user;
	}

	/**
	 * Najde uzivatele podle ID
	 *
	 * @param $id
	 * @return \Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface
	 */
	public function find($id)
	{
		return $this->usersDao->get($id);
	}

	/**
	 * Najde a vrati vsechny uzivatele
	 *
	 * @return Collection<\Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface>
	 */
	public function findAll()
	{
		return $this->usersDao;
	}

	/**
	 * Najde uzivatele podle kriterii
	 *
	 * @param Criteria $criteria
	 * @return Collection<\Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface>
	 */
	public function findBy(Criteria $criteria)
	{
		return $this->usersDao->matching($criteria);
	}
}
