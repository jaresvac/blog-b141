<?php
/**
 * Created by PhpStorm.
 * User: kadleto2
 * Date: 24.9.14
 * Time: 14:46
 */

namespace Cvut\Fit\BiWt1\BlogBundle\Service;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

/**
 * Rozhrani pro CRUD operace s uzivateli
 *
 * Interface UserInterface
 * @package Cvut\Fit\BiWt1\BlogBundle\Service
 */
interface UserInterface {

    /**
     * Vytvori a vrati uzivatele
     *
     * @param $id
     * @param $name
     * @return \Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface
     */
    public function create($id, $name);

    /**
     * Aktualizuje uzivatele a vrati
     *
     * @param \Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface $user
     * @return \Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface
     */
    public function update(\Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface $user);

    /**
     * Odstrani uzivatele a vrati jej
     *
     * @param \Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface $user
     * @return \Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface
     */
    public function delete(\Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface $user);

    /**
     * Najde uzivatele podle ID
     *
     * @param $id
     * @return \Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface
     */
    public function find($id);

    /**
     * Najde a vrati vsechny uzivatele
     *
     * @return Collection<\Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface>
     */
    public function findAll();

    /**
     * Najde uzivatele podle kriterii
     *
     * @param Criteria $criteria
     * @return Collection<\Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface>
     */
    public function findBy(Criteria $criteria);

} 