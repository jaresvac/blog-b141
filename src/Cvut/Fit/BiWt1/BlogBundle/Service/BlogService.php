<?php
/**
 * Created by PhpStorm.
 * User: jares
 * Date: 13/10/14
 * Time: 11:29
 */

namespace Cvut\Fit\BiWt1\BlogBundle\Service;


use Cvut\Fit\BiWt1\BlogBundle\Entity\CommentInterface;
use Cvut\Fit\BiWt1\BlogBundle\Entity\FileInterface;
use Cvut\Fit\BiWt1\BlogBundle\Entity\PostInterface;
use Cvut\Fit\BiWt1\BlogBundle\Entity\TagInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

class BlogService implements BlogInterface
{
	/** @var  ArrayCollection */
	protected $tagsDao;

	/** @var  ArrayCollection */
	protected $postsDao;

	public function __construct()
	{
		$this->tagsDao = new ArrayCollection();
		$this->postsDao = new ArrayCollection();
	}

	/**
	 * Vytvori novy tag, pokud neexistuje
	 *
	 * @param TagInterface $tag
	 * @return TagInterface
	 */
	public function createTag(TagInterface $tag)
	{
		$this->tagsDao->set($tag->getId(), $tag);
		return $tag;
	}

	/**
	 * Upravi stavajici tag
	 *
	 * @param TagInterface $tag
	 * @return TagInterface
	 */
	public function updateTag(TagInterface $tag)
	{
		//wtf?
		return $tag;
	}

	/**
	 * Smaze tag
	 *
	 * @param TagInterface $tag
	 * @return TagInterface
	 */
	public function deleteTag(TagInterface $tag)
	{
		if ($this->tagsDao->contains($tag)) {
			$this->tagsDao->removeElement($tag);
		}
		return $tag;
	}

	/**
	 * Nalezne tag podle ID a vrati
	 *
	 * @param $id
	 * @return TagInterface
	 */
	public function findTag($id)
	{
		return $this->tagsDao->get($id);
	}

	/**
	 * Najde a vrati tagy podle kriterii
	 *
	 * @param Criteria $criteria
	 * @return Collection<TagInterface>
	 */
	public function findTagBy(Criteria $criteria)
	{
		return $this->tagsDao->matching($criteria);
	}

	/**
	 * Vytvori novy zapisek
	 *
	 * @param PostInterface $post
	 * @return PostInterface
	 */
	public function createPost(PostInterface $post)
	{
		$this->postsDao->set($post->getId(), $post);
		return $post;
	}

	/**
	 * Aktualizuje zapisek
	 *
	 * @param PostInterface $post
	 * @return PostInterface
	 */
	public function updatePost(PostInterface $post)
	{
		return $post;
	}

	/**
	 * Smaze zapisek
	 *
	 * @param PostInterface $post
	 * @return PostInterface
	 */
	public function deletePost(PostInterface $post)
	{
		if ($this->postsDao->containsKey($post->getId())) {
			$this->postsDao->remove($post->getId());
		}
		return $post;
	}

	/**
	 * Najde zapisek podle ID a vrati
	 *
	 * @param $id
	 * @return PostInterface
	 */
	public function findPost($id)
	{
		return $this->postsDao->get($id);
	}

	/**
	 * Najde zapisky podle kriterii a vrati
	 *
	 * @param Criteria $criteria
	 * @return Collection<PostInterface>
	 */
	public function findPostBy(Criteria $criteria)
	{
		return $this->postsDao->matching($criteria);
	}

	/**
	 * Prida k zapisku komentar
	 *
	 * @param PostInterface $post
	 * @param CommentInterface $comment
	 * @param CommentInterface $parentComment
	 * @return PostInterface
	 */
	public function addComment(PostInterface $post, CommentInterface $comment,
							   CommentInterface $parentComment = null)
	{
		$comment->setParent($parentComment);
		$post->addComment($comment);
		return $post;
	}

	/**
	 * Odebere od zapisku komentar
	 *
	 * @param CommentInterface $comment
	 * @return PostInterface
	 */
	public function deleteComment(CommentInterface $comment)
	{
		$post = $comment->getPost();
		$post->removeComment($comment);
		return $post;
	}

	/**
	 * Prida k zapisku a pripadne komentari soubor
	 *
	 * @param $file
	 * @param $post
	 * @param $comment
	 * @return PostInterface
	 */
	public function addPostFile(FileInterface $file, PostInterface $post,
								CommentInterface $comment = null)
	{
		$post->addFile($file);
		$comment && $comment->addFile($file);
		return $post;
	}

	/**
	 * Odebere od zapisku soubor
	 *
	 * @param $file
	 * @return PostInterface
	 */
	public function deleteFile($file)
	{
		$file->getComment() && $file->getComment()->removeFile($file);
		$file->getPost()->removeFile($file);
		return $file->getPost();
	}
}
