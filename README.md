# BI-WT1 - Úloha 1 - Blog B141

Šablona pro první úlohu do BI-WT1 v zimním semestru 2014/15.

## Struktura projektu

  * [app/Resources/doc](app/Resources/doc) - Dokumentace k projektu
    * [object_model.puml](../blob/master/object_model.puml) - Objektový model, formát PlantUML
    * [pozadavky.md](../blob/master/app/Resources/doc/pozadavky.md) - Funkční a nefukční požadavky, uživatelské role
    * [zadani.md](../blob/master/app/Resources/doc/zadani.md) - Zadání úlohy 1, hodnocení

## Závislosti projektu

Projekt využívá pro správu závislostí nástroj *[Composer](http://getcomposer.org)*. Abyste mohli projekt začít používat,
je nutné pomoci tohoto nástroje závislosti nainstalovat.

  1. Přihlašte se / vytvořte si účet na serveru http://github.com, vytvořte si
     [API token](https://github.com/settings/applications#personal-access-tokens). Bude potřeba pro bezproblémové fungování
     nástroje *composer* v učebnách a na serveru webdev.fit.cvut.cz.
  1. Přejděte v konzoli do adresáře s projektem.
  1. Upravte soubor `composer.json`, přidejte (pozor, udělejte čárku za poslední vlastností, za kterou přidáváte):

     ```
     "config":{
       "github-oauth":{
         "github.com": "..."
        }
     }
     ```

  1. Poté spusťte:

     ```
     curl -sS https://getcomposer.org/installer | php;
     php composer.phar update;
     ```
