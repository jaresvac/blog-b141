# Požadavky

## Uživatelské role

  * R.1 Nepřihlášený uživatel
  * R.2 Přihlášený uživatel - autentizuje se proti lokální databázi uživatelů (v tomto případě proti MySQL)
    * R.2.1 Autor
    * R.2.2 Čtenář
    * R.2.3 Správce

## Funkční požadavky

  * F.1 Autentizace uživatele
    * F.1.1 Přihlášení uživatele
    * F.1.2 Odhlášení uživatele
  * F.2 Zobrazení příspěvků
    * F.2.1 Filtr podle autora
    * F.2.2 Filtr podle měsíce zveřejnění
    * F.2.3 Filtr podle tagů
    * F.2.4 Podpora zobrazení pouze v zadaném intervalu (published from - to)
  * F.3 Vkládání příspěvků
    * F.3.1 Přílohy příspěvků
  * F.4 Vkládání komentářů
    * F.4.1 Přílohy komentářů
    * F.4.2 Reakce na komentář (hierarchické zobrazení)

## Nefunkční požadavky

  1. PHP
  1. Použití doctrine/common, Collection, Criteria
  1. Testování PHPunit
