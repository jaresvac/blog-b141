# BI-WT1 - Úloha 1 - Blog B141 - zadání a hodnocení

## Zadání

Implementujte datový model a služby pro aplikaci blog podle připravených rozhraní:

  * Cvut\Fit\BiWt1\BlogBundle\Entity\ImageInterface
  * Cvut\Fit\BiWt1\BlogBundle\Entity\UserInterface
  * Cvut\Fit\BiWt1\BlogBundle\Entity\PostInterface
  * Cvut\Fit\BiWt1\BlogBundle\Entity\TagInterface
  * Cvut\Fit\BiWt1\BlogBundle\Entity\FileInterface
  * Cvut\Fit\BiWt1\BlogBundle\Entity\CommentInterface
  * Cvut\Fit\BiWt1\BlogBundle\Service\BlogInterface
  * Cvut\Fit\BiWt1\BlogBundle\Service\UserInterface

## Hodnocení

Úloha bude hodnocena automatizovaným testem (jednotkové testy). Bodový výsledek bude určen vztahem: (počet bodů za úlohu * počet úspěšných testů / počet testů celkem).

Úloha bude hodnocena 0 body, pokud bude splněna libovolná z následujících podmínek:

  * Úloha je předložena k hodnocení po termínu uvedeném na EDUXu. Toto neplatí v odůvodněných případech (např. nemoc).
  * Z logu nástroje git není patrná průběžná práce (tj. log bude obsahovat minimum záznamů) bude hodnocena 0 body. Posouzení přísluší osobě vyučujícího!
  * Z logu nástroje git není možno jasně identifikovat osobu (osoby), která(é)  prováděla(y) většinu změn. Za vhodnou identifikaci považujeme jméno, příjmení a fakultní e-mailovou adresu.

Přístup do repozitáře musí být povolen pro *Deploy Key* (Settings -> Deploy Keys):

```
ssh-dss AAAAB3NzaC1kc3MAAACBANpZpTpv/SzJ2CDYPFj8HYZ+K6uKgLgkNEdU/V3H9r7CzWqwyNPei1bjPkpXr4Zow01PwbkjTtQ/jH2GyCwTURJkZBSPuePMGZqgZpfKsrqmrRVBnGmzRTrx6OccHFgvCV02U2yx06AfjEZfPqiJ8OvEFEdsNmVCKU2TmUaDWDexAAAAFQDsaOS9uZYJ5BYIqqwaxXTZUjqzewAAAIA8lNZ+uLdHUmfFdfOch7UXu6M/CC0YiKHX3+GuwYL1+BFBb0lATBNlw0rIk1Opq7O9TJ/fWqj+C5Z4GXPNTIISq2g5azPN+6JiQIl4j84gSrr/rZb1gcSrmKmJQdDBzo1aDecgIIM/qaoQLqMimbpman7A381CKXWva0HMxir3GgAAAIEA1+tRVR9MK454KrOPOm0ORRq8nddPuMh1nVe7sbyFnx88eM1Ovio+t/FSA23u91DP26VlHh3yEI2Zjd3g+TYf0F8hqTnLGjVTSnApturndJC5ZZ960Fu4NFs5tMcg8GMN0L9R/ojUAuoraZkPoISUr0JpdcVhZU7UrH9t7hrwqeE= kadleto2@webdev-fit-01
```

## Termín hodnocení

Automatické vyhodnocení bude spuštěno 13. 11. 2014 v 0.00, aby bylo možné na cvičeních hodnocení uzavřít. Toto je změna oproti zveřejněnému hodnocení na EDUXu (13. 11. 2014 na cvičení). Nepředpokládáme však, že by vám měla působit komplikace.

## Postup vyhodnocení

  1. Z vašeho repozitáře (https://gitlab.fit.cvut.cz/LOGIN/blog-b141) bude staženo vaše řešení.
  1. Budou porovnány třídy testů ve vašem projektu a v zadání.
  1. Bude doplněn soubor composer.json o klíč pro GitHub, pokud jej nebude již obsahovat.
  1. Budou stažen nástroj composer a pomocí něj nainstalovány závislosti.
  1. Budou spuštěny testy a vypočítáno hodnocení (vizte výše).

Pokud jakákoliv z popsaných operací skončí chybou zaviňenou vámi (např. nedodržení zadání, neošetřené chyby v kódu), znamená to hodnocení vašeho řešení 1. úlohy 0 body.

Celý postup vyhodnocení bude zalogován, log bude k nahlédnutí na cvičení.
