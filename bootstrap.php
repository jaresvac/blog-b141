<?php
/**
 * Zajistuje inicializaci autoloaderu a objektu pro mapovani rozhrani na jejich implementace
 *
 * Created by PhpStorm.
 * User: kadleto2
 * Date: 1.10.14
 * Time: 17:09
 */

require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/cli-config.php');
$interfaceToClassMap =
    new \Cvut\Fit\BiWt1\BlogBundle\DependencyInjection\InterfaceToClassMap(__DIR__ . '/app/config/classes.yml');