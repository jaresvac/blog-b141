#!/bin/bash

function onError {
    echo "*** Doslo k chybe, vyhodnoceni konci" 1>&2
    [ -n "$OLDPWD" ] && cd -
    exit
}

function onExit {
        echo "*** CELKEM BODU: $BODY"
        trap - EXIT INT QUIT ABRT TERM HUP
}

trap onExit EXIT INT QUIT ABRT TERM HUP
trap onError ERR

function log {
    local severity="${2:-ERROR}"
    local message="${1:-Zpráva nezadána ...}"
    echo "*** $severity: $message" 1>&2
    [ "$severity" = 'ERROR' ] && false || true
}

GITLAB='git@gitlab.fit.cvut.cz:LOGIN/blog-b141.git'
TESTDIR='.'
ZADANI="$PWD"
BODY=0
GITHUB_KEY="$2"

# neprazdny login - stahni, jinak je to v aktualnim adresari
[ -n "$1" ] && id "$1" 1>/dev/null 2>/dev/null && {
    TESTDIR="/tmp/$USER"
    mkdir -p "$TESTDIR"
    chmod 700 "$TESTDIR"
    
    exec 1>"$TESTDIR/$1.log" 2>&1

    TESTDIR="/tmp/$USER/$1"
    git clone "${GITLAB/LOGIN/$1}" "$TESTDIR";
}

cd "$TESTDIR"

[ -z "$GITHUB_KEY" ] && 
    log "Neni nastaven klic pro github, muze nastat problem." "WARNING"

# porovnani souboru zadani se soubory v reseni
KONZISTENCE=1

# z jedne strany
while read DFILE; do
    SFILE="$ZADANI/$DFILE"
    diff -u "$DFILE" "$SFILE" || KONZISTENCE=0;
done < <(find "src/Cvut/Fit/BiWt1/BlogBundle/Tests" -type f -iname '*Test*.php')

# z druhe strany
while read SFILE; do
    DFILE=$(echo "$SFILE" | sed "s|^$ZADANI/||")
    diff -u "$SFILE" "$DFILE" || KONZISTENCE=0;
done < <(find "$ZADANI/src/Cvut/Fit/BiWt1/BlogBundle/Tests" -type f -iname '*Test*.php')

(( KONZISTENCE == 0 )) && log "Tridy testu se lisi od zadani."

# overeni existence composer.json
[ ! -f "composer.json" ] && log "Neexistuje composer.json"

# stazeni composer.phar
[ ! -f "composer.phar" -o -x "composer.phar" ] &&
    curl -sS https://getcomposer.org/installer | php

# doplneni github klice
[ -n "$GITHUB_KEY" ] && {
    php <<- --PHP--
    <?php
    \$composer = json_decode(file_get_contents('composer.json'), true);

    if (!isset(\$composer['config']['github-oauth']['github.com'])) {
        \$composer['config']['github-oauth']['github.com'] = "$GITHUB_KEY";
    }

    file_put_contents('composer.new.json', json_encode(\$composer, JSON_PRETTY_PRINT));
--PHP--
    php composer.phar validate composer.new.json;
    \mv -f composer.new.json composer.json;
}

# stazeni zavislosti
# bylo by lepsi install - respektuje pripadny composer.lock
php composer.phar update

# spusteni testu
vendor/bin/phpunit src/Cvut/Fit/BiWt1/BlogBundle/Tests || true
USPESNYCH=$(vendor/bin/phpunit --tap src/Cvut/Fit/BiWt1/BlogBundle/Tests | grep '^ok' | grep -v '# SKIP' | wc -l)
BODY=$( echo "15 * $USPESNYCH / 63" | bc )

echo '-----COMMIT--------'
git shortlog -se | cat
echo '-------------------'

cd - 1>/dev/null 2>&1
