<?php
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Cache\ApcCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Tools\Setup;

require_once 'vendor/autoload.php';

$dbParams = array(
	'driver' => 'pdo_mysql',
	'user' => 'zadejte Váš login',
	'password' => 'zadejte Vaše heslo',
	'host' => 'webdev.fit.cvut.cz',
	'port' => '3306',
	'dbname' => 'zadejte Váš login',
	'unix_socket' => '/tmp/mysql.sock',
);

$paths = [ __DIR__ . '/src/Cvut/Fit/BiWt1/BlogBundle/Entity' ];
$proxyDir = __DIR__ . '/app/cache/Proxies';

$cache = new ArrayCache;
//$cache = new ApcCache;

$doctrineConfig = Setup::createAnnotationMetadataConfiguration($paths, FALSE, $proxyDir, $cache);
$doctrineConfig->setAutoGenerateProxyClasses(true);

// alter metadata driver impl
$reader = new AnnotationReader();
$driverImpl = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver($reader, array(__DIR__. '/src/Cvut/Fit/BiWt1/BlogBundle/Entity'));
$doctrineConfig->setMetadataDriverImpl($driverImpl);

$em = EntityManager::create($dbParams, $doctrineConfig);

return ConsoleRunner::createHelperSet($em);